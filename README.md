# Light curve of HD189733

A small project in C and Python to produce a light curve showing the transit of the exoplanet HD189733b.

Files of this project:
- main.c : main program used to produce a text file containing the julian day and the flux of HD189733
- cfits.c : functions to open and read the fits files
- numlin.c : functions to create and perform operations on 2D arrays
- photometry.c : functions to find the centroid of the stars and perform the aperture photometry on the stars


Commands to compile and execute the program to produce the light curve :
```bash
gcc -I include numlin.c cfits.c photometry.c main.c -lm -o main
./main <folder with fits>
python3.8 plot.py
```

<img src="lc_hd189733.png" alt="drawing" width="450"/>
