#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <dirent.h>
#include "photometry.h"
#include "numlin.h"

void photometry(long int **img, double x_fit, double y_fit ,int x_int,int y_int,int box_size,int x0,int y0, int radius, int r_in,int r_out,double *srcptr,double *bkgptr){
	/*  Function performing an aperture photometry on an image. 
	 *  The sources are considered circular. The photons are counted in apertures using subpixels.
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **img    : long int : 2D-array of size (naxis1,naxis2) in which the image will be stored 
	 *  x_fit    : double   : value obtained through fit of the position of the star
	 *  y_fit    : double   : value obtained through fit of the position of the star
	 *  x_int    : int      : integer value of the position of the star 
	 *  y_int    : int 	    : integer value the position of the star
	 *  box_size : int      : size of the array of truncated
	 *  x0		 : int      : intial value for the position of the source 
	 *  y0       : int      : intial value for the position of the source 
	 *  radius   : int      : radius of the circle 
	 *  r_in     : int      : radius of the inner of the annulus
	 *  r_out    : int      : radius of the outer of the annulus
	 *  *srcptr  : double   : pointer to the number of photons in the source
	 *  *bkgptr  : double   : pointer to the number of photons in the background
	 * 
	 */
	
	
	int incr=2; 
	int subpixels=50; //
	int new_box_size=box_size/2+incr;
	int origin_pixel=2*new_box_size;
	int sub_size=origin_pixel*subpixels;
	double **X_sub,**Y_sub,**sub_data;
	long int **data;
	double x_aperture=0.;
	double y_aperture=0.;
	double lhs;
	double source_cts=0;
	double background_cts=0;
	
	data=create_array_int(origin_pixel,origin_pixel);
	X_sub=create_grids(0,sub_size-1,sub_size,sub_size,0);
	Y_sub=create_grids(0,sub_size-1,sub_size,sub_size,1);
	sub_data=create_array(sub_size,sub_size);
	truncate_matrix(data,img,x_int+box_size/2,y_int+box_size/2,new_box_size*2);
	
	int x_p=x0-box_size/2+x_fit;
	int y_p=y0-box_size/2+y_fit;

	//initialize the subdata array
	for (int k=0;k<sub_size;k++){ 
		for (int l=0;l<sub_size;l++){
			sub_data[k][l]=0.;}}	
	// filling the subdata array 
	for (int r=0;r<origin_pixel;r++){
		for (int t=0;t<origin_pixel;t++){
			for (int ix=subpixels*r;ix<subpixels*(1+r);ix++){
				for (int iy=subpixels*t;iy<subpixels*(1+t);iy++){
					sub_data[ix][iy]+=data[r][t]/(1.*subpixels*subpixels);
				}
			}
		}
	}
	x_aperture=(x_fit+incr+x0-x_p)*subpixels+(subpixels-1)/2;
	y_aperture=(y_fit+incr+y0-y_p)*subpixels+(subpixels-1)/2;

	//sum on the apertures (circular and annulus)
	for (int ii=0; ii<(sub_size);ii++){
		for (int jj=0;jj<(sub_size);jj++){

			lhs=sqrt((X_sub[ii][jj]-x_aperture)*(X_sub[ii][jj]-x_aperture)+(Y_sub[ii][jj]-y_aperture)*(Y_sub[ii][jj]-y_aperture));
			if (lhs<=radius*subpixels){
				source_cts+=sub_data[ii][jj];
			}
			if ((lhs<=(r_out*subpixels)) && (lhs>=(r_in*subpixels))){
				background_cts+=sub_data[ii][jj];
			}
		}
	}
	*bkgptr=background_cts;
	*srcptr=source_cts;

	delete_array(sub_data,sub_size);
	delete_array(X_sub,sub_size);
	delete_array(Y_sub,sub_size);
	delete_array_int(data,origin_pixel);
}

void Fit_Gauss2DLevenbergMarquardt(double **params_curr,double **X,double **Y,long int **truncated, int box_size){
    /*  Fit a 2D Gaussian plus a constant on a source + background using the Levenberg Marquardt method 
	 *  presented in the 2nd Edition of Numerical Recipes in C, Chapter 15.5 Nonlinear models pages 681-687.
	 *  Note that it is a naive implementation.
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **params_curr : double   : array of size (6,1) in which the parameter of the fit will be stored
	 *  **X           : double   : array of size (box_size,box_size) with the values of the grid in the X axis
	 *  **Y           : double   : array of size (box_size,box_size) with the values of the grid in the Y axis
	 *  **truncated   : long int : array of size (box_size,box_size) truncated image of the original fits image
	 *  box_size      : int 	 : size of the array of truncated
	 * 
	 */
	
	double lambda=0.001, mu=10.;
	double chi2_next,chi2_curr=0.;
	double val_max,val_min=0;
	int n_iter=30;
	int indexes[2]={0,0};
	double **params_step=create_array(6,1);
	double **params_next=create_array(6,1);
	double **G=create_array(6,1);
	double **H=create_array(6,6);
	
	find_min_int(truncated,box_size,&val_min,indexes);//first because will be overwritten by find_max_int
	find_max_int(truncated,box_size,&val_max,indexes);
	
	params_curr[0][0]=val_min;
	params_curr[1][0]=val_max;
	params_curr[2][0]=indexes[1];
	params_curr[3][0]=indexes[0];
	params_curr[4][0]=0.5;
	params_curr[5][0]=0.5;
	
	for (int i=0;i<n_iter;i++){
		GradientGaussian2D(G,X,Y,params_curr,truncated,box_size);
		HessianGaussian2D(H,X,Y,params_curr,truncated,box_size);
		for (int j=0;j<6;j++){
			for (int k=0;k<6;k++){
				if (j==k){H[j][k]+=lambda;}}}
		solve_system(params_step,H,G,6);
		if (params_step[0][0]==-1 && params_step[1][0]==-1 && params_step[2][0]==-1  && params_step[3][0]==-1  && params_step[4][0]==-1  && params_step[5][0]==-1 ){
			printf("Stop the fit because system is not solvable !\n");
			break;
		}
		for (int j=0;j<6;j++){
			params_next[j][0]=params_curr[j][0]+params_step[j][0];}
		chi2_next=chisquareGaussian2D(truncated,X,Y,params_next,box_size);
		chi2_curr=chisquareGaussian2D(truncated,X,Y,params_curr,box_size);
		if (chi2_next<=chi2_curr){
			lambda/=mu;
			for (int j=0;j<6;j++){params_curr[j][0]=params_next[j][0];}}
		else {
			lambda*=mu;}
	}
	delete_array(params_step,6);
	delete_array(params_next,6);
	delete_array(H,6);
	delete_array(G,6);
	if (params_curr[2][0]>box_size || params_curr[2][0]<0 || params_curr[3][0]>box_size || params_curr[3][0]<0){
		params_curr[2][0]=indexes[1];	//	this is mostly to return values of the max if the fit doesn't
		params_curr[3][0]=indexes[0];	//	converge to an acceptable value 
	}
}

double chisquareGaussian2D(long int **data, double **X,double **Y,double **parms_k,int n){
    /* 	 chi square of the 2D Gaussian plus a constant 
	 * 
	 *  It is the function :
	 *  Chi2(B,A,x0,y0,sig_x,sig_y)= sum ( ((data-model)/sigma)^2)
	 *  where model is the 2D gaussian + constant: B+A exp( -(x-x0)^2/(2*sig_x^2) - (y-y0)^2/(2*sig_y^2))
	 * 	And sigma is the error on the data, here we consider that the error is in sqrt(data).
	 *
	 *  Parameters
	 *  ----------
	 * 
	 *  **data    : long int : array of size (n,n) with the data to fit the Gaussian.
	 *  **X       : double   : array of size (n,n) with the values of the grid in the X axis
	 *  **Y       : double   : array of size (n,n) with the values of the grid in the Y axis
	 *  **parms_k : double   : array of size (6,1) with the values of the parameters of the Gaussian in the order  : B , A , x0 , y0 , sig_x , sig_y 
	 *  n         : int 	 : size of the array of data 
	 * 
	 */
	
	double C=0;
	double model;
	double B=parms_k[0][0];
	double A=parms_k[1][0];
	double x_0=parms_k[2][0];
	double y_0=parms_k[3][0];
	double sig_x=parms_k[4][0];
	double sig_y =parms_k[5][0];
	
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			model=B+A * exp( -powf(X[i][j]-x_0,2.) / (2*powf(sig_x,2.)) - powf(Y[i][j]-y_0,2.) / (2*powf(sig_y,2.)) );
			C+=pow(data[i][j]-model,2.)/data[i][j];
		}
	}
	return C;
}

void HessianGaussian2D(double **H,double **X,double **Y,double **parms_k,long int  **data,int n){
	/* 	 Hessian of the chi square of the 2D Gaussian plus a constant 
	 * 
	 *  It is the Hessian of the function :
	 *  Chi2(B,A,x0,y0,sig_x,sig_y)= sum ( ((data-model)/sigma)^2)
	 *  where model is the 2D gaussian + constant: B+A exp( -(x-x0)^2/(2*sig_x^2) - (y-y0)^2/(2*sig_y^2))
	 * 	And sigma is the error on the data, here we consider that the error is in sqrt(data).
	 *
	 *  Parameters
	 *  ----------
	 * 
	 *  **H       : double   : array of size (6,1) in which the Hessian will be stored 
	 *  **X       : double   : array of size (n,n) with the values of the grid in the X axis
	 *  **Y       : double   : array of size (n,n) with the values of the grid in the Y axis
	 *  **parms_k : double   : array of size (6,1) with the values of the parameters of the Gaussian in the order  : B , A , x0 , y0 , sig_x , sig_y 
	 * 	**data    : long int : array of size (n,n) with the data to fit the Gaussian.
	 *  n         : int 	 : size of the array of data 
	 * 
	 */
	double A=parms_k[1][0];
	double x_0=parms_k[2][0];
	double y_0=parms_k[3][0];
	double sig_x=parms_k[4][0];
	double sig_y =parms_k[5][0];
	double model,val;

	for (int i=0;i<6;i++){
		for (int j=0;j<6;j++){
				H[i][j]=0.;
		}
	}
	
	double sigx3,sigy3,sigy2,sigx2,x_x0,y_y0,x_x02,y_y02;	
	sigx3=sig_x*sig_x*sig_x;
	sigx2=sig_x*sig_x;
	sigy3=sig_y*sig_y*sig_y;
	sigy2=sig_y*sig_y;
	
	H[0][1]=H[1][0]=0.;
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			x_x0=X[i][j]-x_0 ;
			x_x02=(X[i][j]-x_0)*(X[i][j]-x_0);
			y_y0=Y[i][j]-y_0;
			y_y02=(Y[i][j]-y_0)*(Y[i][j]-y_0);
			model=A * exp( -x_x02 / (2*sigx2) - y_y02 / (2*sigy2));
			val=data[i][j];
			
			H[0][0]=1./val;
			H[1][1]+=model*model/(A*A)/val;
			H[2][2]+=(x_x0 * model  / sigx2 )*(x_x0 * model  / sigx2 )/val;
			H[3][3]+=(y_y0 * model  / sigy2)*(y_y0 * model  / sigy2)/val;
			H[4][4]+=(x_x02 * model  / sigx3)*(x_x02 * model  / sigx3)/val;
			H[5][5]+=(y_y02 * model  / sigy3)*(y_y02 * model  / sigy3)/val;
		
			H[0][2]+=model * x_x0 * model  / sigx2/val;
			H[0][3]+=model * y_y0 * model  / sigy2 /val;
			H[0][4]+=model * x_x02 * model  / sigx3 /val;
			H[0][5]+=model * y_y02 * model  / sigy3 /val;
			
			H[1][2]+=model/A * x_x0 * model  / sigx2/val;
			H[1][3]+=model/A * y_y0 * model  / sigy2/val;
			H[1][4]+=model/A * x_x02 * model  / sigx3/val;
			H[1][5]+=model/A * y_y02 * model  / sigy3/val;
			
			H[2][3]+=x_x0 * model  / sigx2 * y_y0 * model  / sigy2/val;
			H[2][4]+=x_x0 * model  / sigx2 * x_x02 * model  / sigx3/val;
			H[2][5]+=x_x0 * model  / sigx2 * y_y02 * model  / sigy3 /val;
			
			H[3][4]+=y_y0 * model  / sigy2 * x_x02 * model  / sigx3/val;
			H[3][5]+=y_y0 * model  / sigy2 * y_y02 * model  / sigy3/val;
			
			H[4][5]+=x_x02 * model  / sigx3* y_y02 * model  / sigy3/val;
		}

	}	H[5][4]=H[4][5];
		
		H[4][3]=H[3][4];
		H[5][3]=H[3][5];
		
		H[3][2]=H[2][3];
		H[4][2]=H[2][4];
		H[5][2]=H[2][5];
		
		H[2][1]=H[1][2];
		H[3][1]=H[1][3];
		H[4][1]=H[1][4];
		H[5][1]=H[1][5];

		H[5][0]=H[0][5];
		H[4][0]=H[0][4];
		H[3][0]=H[0][3];
		H[2][0]=H[0][2];
}

void GradientGaussian2D(double **G,double **X,double **Y,double **parms_k,long int  **data,int n){
	/* 	Gradient of the chi square of the 2D Gaussian plus a constant 
	 * 
	 *  It is the gradient of the function :
	 *  Chi2(B,A,x0,y0,sig_x,sig_y)= sum ( ((data-model)/sigma)^2)
	 *  where model is the 2D gaussian + constant: B+A exp( -(x-x0)^2/(2*sig_x^2) - (y-y0)^2/(2*sig_y^2))
	 * 	And sigma is the error on the data, here we consider that the error is in sqrt(data).
	 *
	 *  Parameters
	 *  ----------
	 * 
	 *  **G       : double   : array of size (6,1) in which the gradient will be stored 
	 *  **X       : double   : array of size (n,n) with the values of the grid in the X axis
	 *  **Y       : double   : array of size (n,n) with the values of the grid in the Y axis
	 *  **parms_k : double   : array of size (6,1) with the values of the parameters of the Gaussian in the order  : B , A , x0 , y0 , sig_x , sig_y 
	 * 	**data    : long int : array of size (n,n) with the data to fit the Gaussian.
	 *  n         : int 	 : size of the array of data 
	 * 
	 */
	double B=parms_k[0][0];
	double A=parms_k[1][0];
	double x_0=parms_k[2][0];
	double y_0=parms_k[3][0];
	double sig_x=parms_k[4][0];
	double sig_y =parms_k[5][0];
	double val;
	double model;
	G[0][0]=G[1][0]=G[2][0]=G[3][0]=G[4][0]=G[5][0]=0.;
	
	
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			model=B + A * exp( -pow(X[i][j]-x_0,2.) / (2*pow(sig_x,2.)) - pow(Y[i][j]-y_0,2.) / (2*pow(sig_y,2.)) );
			val=data[i][j];
			/* it is +2 because we need -G */
			G[0][0]+=2 * (val-model)/val ;
			G[1][0]+=2 * (val-model) * (model-B) / A /val;
			G[2][0]+=2 * (val-model) * (X[i][j]-x_0) * (model-B)  / pow(sig_x,2.) /val;
			G[3][0]+=2 * (val-model) * (Y[i][j]-y_0) * (model-B)  / pow(sig_y,2.) /val;
			G[4][0]+=2 * (val-model) * pow(X[i][j]-x_0,2.) * (model-B)  / pow(sig_x,3.) /val;
			G[5][0]+=2 * (val-model) * pow(Y[i][j]-y_0,2.) * (model-B)  / pow(sig_y,3.) /val;
		}
	}
}
