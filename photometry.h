#ifndef PHOTOMETRY_H_INCLUDED
#define PHOTOMETRY_H_INCLUDED

void HessianGaussian2D(double **H,double **X,double **Y,double **parms_k,long int  **data,int n);
void GradientGaussian2D(double **G,double **X,double **Y,double **parms_k,long int  **data,int n);
double chisquareGaussian2D(long int **data, double **X,double **Y,double **parms_k,int n);
void Fit_Gauss2DLevenbergMarquardt(double **params_curr,double **X,double **Y,long int **truncated, int box_size);

void photometry(long int  **img, double x_fit, double y_fit ,int x_int,int y_int,int box_size,int x0,int y0, int radius, int r_in,int r_out,double *srcptr,double *bkgptr);
#endif
