#ifndef CFITS_H_INCLUDED
#define CFITS_H_INCLUDED

#define BLOCK_SIZE 36
#define KEYWORD_SIZE 80
#define HEADER_BLOCK 2880

double retrieve_header_double(char *header_buff, char *keyword_name);
void get_data_image(long int **img, char *file_name,long int naxis1, long int naxis2, long int bzero, double bscale, int size_header);
u_int32_t swap_u32b_big2little(int big);
long int retrieve_header_int(char *header_buff, char *keyword_name);
void get_header_values(char *file_name,long int *naxis1ptr, long int *naxis2ptr,long int *bzeroptr, int *size_headerptr,double *bscaleptr,double *jdptr);
#endif
