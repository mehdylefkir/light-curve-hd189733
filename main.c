#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <dirent.h>
#include "numlin.h"
#include "cfits.h"
#include "photometry.h"


int main(int argc, char *argv[]) {
	/*  Main function to produce the light curve of HD189733
	 * 
	 *  Compile with :  gcc -I include numlin.c cfits.c photometry.c main.c -lm -o main
	 *  Excute with  : ./main <folder with fits>
	 * 
	 *  
	 *  The folder containing the fits files must be passed as an argument like :
	 *   
	 *  ./main-bin HD189733/
	 * 
	 */

	DIR *folder;
	struct dirent *entry;
	folder = opendir(argv[1]);
	char file_name[255];
	FILE *fp;	
	fp=fopen("results.txt","w+");
	
	int number_of_stars=5;
	int x0[]={90,99,280,251,299}; // initial values for the stars
	int y0[]={264,442,267,257,407};// initial values for the stars
	
	int radius=3;
	int r_out=8;
	int r_in=5;
	int box_size=30;
	int files=0;
	int size_header;
	long int naxis1,naxis2,bzero;
	
	double x_fit,y_fit,bscale,jd;
	double srcptr=0., bkgptr=0., src_area=0, bkg_area=0,mean=0, corrected=0.;
	
	double **src=create_array(number_of_stars,1);
	double **bkg=create_array(number_of_stars,1);	
	double **src_bkg_sub=create_array(number_of_stars,1);	
	double **X=create_grids(0,box_size-1,box_size,box_size,0);
	double **Y=create_grids(0,box_size-1,box_size,box_size,1);	
	double **params=create_array(6,1);
	long int **truncated=create_array_int(box_size,box_size);
	long int **img;
	
	if(folder == NULL)
    {   printf("Unable to open directory\n");
        return(-1);}
	else{
		while( (entry=readdir(folder)) ){
			files++;
			if (files>2 && files<150){
				strncpy(file_name,entry->d_name,sizeof(file_name));
				char *tmp = strdup(file_name);
				strcpy(file_name,argv[1]); strcat(file_name, tmp);
				printf("File %d : %s ",files-2,file_name);
				get_header_values(file_name,&naxis1,&naxis2,&bzero, &size_header,&bscale,&jd);
				
				if (files==3){ 
					img=create_array_int(naxis1,naxis2);
					for (int i=0;i<naxis1;i++){
						for (int j=0;j<naxis2;j++){
							img[i][j]=0;}}
				} // to initialize only once the array
				get_data_image(img,file_name,naxis1,naxis2,bzero, bscale,size_header);
				
				for (int star=0;star<number_of_stars;star++){
					bkg[star][0]=0.; src[star][0]=0.;src_bkg_sub[star][0]=0.;	
					
					/*------------ FIT OF a 2D Gaussian -------------------*/
					truncate_matrix(truncated,img,x0[star],y0[star],box_size);
					Fit_Gauss2DLevenbergMarquardt(params,X,Y,truncated, box_size);
					x_fit=params[2][0];
					y_fit=params[3][0];			
					double posx=x0[star]-box_size+x_fit;
					double posy=y0[star]-box_size+y_fit;
					int x_int=posx;
					int y_int=posy;
					
					/*------------ Perform the photometry aperture -------------------*/
					photometry(img, x_fit, y_fit ,x_int,y_int,box_size,x0[star],y0[star],radius,r_in,r_out,&srcptr,&bkgptr);
					bkg[star][0]=bkgptr;
					src[star][0]=srcptr;
				}
				src_area=M_PI*radius*radius;
				bkg_area=M_PI*(r_out*r_out-r_in*r_in);	
				mean=0;
				for (int star=0;star<number_of_stars;star++){
					src_bkg_sub[star][0]=src[star][0]-bkg[star][0] *src_area/bkg_area;
					if (star>0){mean+=src_bkg_sub[star][0];}
				}
				corrected=src_bkg_sub[0][0]/(mean/(number_of_stars-1.));
				printf("value : %lg\n",corrected);
				fprintf(fp,"%10.15lg\t%10.15lg\n",jd,corrected);
			}
		}
	}
	delete_array(src_bkg_sub,number_of_stars);
	delete_array(src,number_of_stars);	
	delete_array(bkg,number_of_stars);
	delete_array(params,6);
	delete_array(X,box_size);
	delete_array(Y,box_size);
	delete_array_int(truncated,box_size);
	delete_array_int(img,naxis1);
	fclose(fp);
	closedir(folder);
	return 0;
}
