#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cfits.h"

void get_header_values(char *file_name,long int *naxis1ptr, long int *naxis2ptr,long int *bzeroptr, int *size_headerptr,double *bscaleptr,double *jdptr){
	/*  Function to get values from the header of a fits file.
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  *file_name     : str      : name of the fits file  
	 *  *naxis1ptr     : long int : pointer to get the size of the first dimension
	 *  *naxis2ptr     : long int : pointer to get the size of the second dimension
	 *  *bzeroptr      : long int : pointer to get the bzero value
	 * 	*bscaleptr     : double   : pointer to get the bscale value
	 *  *size_headerptr: int 	  : pointer to get the size of the header in bytes
	 * 	*jdptr         : double   : pointer to get the julian day
	 * 
	 */
	 
	char character;
	int i = 1;
	int nb_lines, nb_blocks = 0;
	long length;
	
	char *p = NULL;
	FILE *fp = fopen (file_name, "r");

	fseek (fp, 0, SEEK_END);
	length = ftell (fp);
	fseek (fp, 0, SEEK_SET);
	char header_buff[length];
	memset(header_buff,0,strlen(header_buff));
	if ( fp == NULL )
	{ printf("Couldn't open the file !");}
	else
	{
		while (1){
			character=fgetc(fp);
			if (character == EOF || ((p != NULL) && nb_lines%BLOCK_SIZE == 0)) {
				break;
			}
			else {
				if (i == KEYWORD_SIZE){
					i = 1;
					nb_lines++;
					if (nb_lines%BLOCK_SIZE == 0) {nb_blocks++;}}
				else {i++;}
				strncat(header_buff,&character,1);
			}
			p=strstr(header_buff,"END");
		}
		int size_header = BLOCK_SIZE*nb_blocks*KEYWORD_SIZE;
		long int naxis1 = retrieve_header_int(header_buff,"NAXIS1");
		long int naxis2 = retrieve_header_int(header_buff,"NAXIS2");
		long int bzero = retrieve_header_int(header_buff,"BZERO");
		double bscale = retrieve_header_double(header_buff,"BSCALE");	
		double jd = retrieve_header_double(header_buff,"JD");	
		
		*size_headerptr = size_header;
		*bzeroptr = bzero;
		*bscaleptr = bscale;
		*naxis1ptr = naxis1;
		*naxis2ptr = naxis2;
		*jdptr = jd;
	}
}

void get_data_image(long int **img, char *file_name,long int naxis1, long int naxis2, long int bzero, double bscale, int size_header){
	/*  Retrieve an image of dimension naxis1*naxis2 in a fits file with data in 32bits
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **img      : long int : 2D-array of size (naxis1,naxis2) in which the image will be stored 
	 *  *file_name : str      : name of the fits file  
	 *  naxis1     : long int : size of the first dimension
	 *  naxis2     : long int : size of the second dimension
	 *  bzero      : long int : used to get the physical value = bzero+int32*bscale
	 * 	bscale     : double   : used to get the physical value = bzero+int32*bscale
	 *  size_header: int 	  : size of the header in bytes
	 * 
	 */
	
	FILE *fp = fopen (file_name, "r");
	if ( fp == NULL )
	{printf("Couldn't open the file !");}
	else
	{
		int32_t buff;
		fseek (fp, 0, SEEK_SET);
		fseek (fp, size_header+1, SEEK_SET);
		for (int i=0;i<naxis1;i++){
			for (int j=0;j<naxis2;j++){
				fread(&buff,sizeof(buff),1,fp);
				u_int32_t num = swap_u32b_big2little(buff*bscale+bzero)/256;
				img[i][j] = num ;
			}
		}
	fclose(fp);
	}	
}

u_int32_t swap_u32b_big2little(int big){
	/* Converts 32bits values in big endian to little endian
	 * 
	 * Parameters
	 * ----------
	 * 
	 * big : int : integer in big endian
	 * 
	 */
	int n = 1;
	if ( *(char *)&n == 1) { //if the system is little-endian we have to convert the values from the data which are in big-endian
		u_int32_t num;
		char *big_ptr = (char *)&big;
		char *little_ptr = (char *)&num;
		little_ptr[0] = big_ptr[3];
		little_ptr[1] = big_ptr[2];
		little_ptr[2] = big_ptr[1];
		little_ptr[3] = big_ptr[0];
		return num;
	}
	else return big;
}

long int retrieve_header_int(char *header_buff, char *keyword_name){
	/*  Function used to retrieve long int values from the header 
	 * 	 
	 * 	Parameters
	 *  ----------
	 * 
	 *  header_buff : string containing the header
	 *	keyword_name : string containing the keyword
	 * 
     */
	char keybuff[strlen(keyword_name)];
	strcpy(keybuff,keyword_name);

	while (strlen(keybuff)<8){strcat(keybuff," ");}
	strcat(keybuff,"= ");
	char buffer[KEYWORD_SIZE]="";
	char *p=strstr(header_buff,keybuff);

	for (int i = 0;i < KEYWORD_SIZE; i++){
		if (i>8 && p[i]!=*"/"){	strncat(buffer,&p[i],1);}
		else if (p[i]==*"/"){break;}
		}
	char *ptr=NULL;
	long int keyword=strtol(buffer,&ptr,10);
	
	if (ptr==buffer) {printf("String couldn't be converted to long int\n");return -1;}
	else {	return keyword;}
}

double retrieve_header_double(char *header_buff, char *keyword_name){
	/*  Function used to retrieve double values from the header
	 * 
	 * 	Parameters
	 *  ----------
	 * 
	 *  header_buff : string containing the header
	 *	keyword_name : string containing the keyword
	 * 
	 */
	char keybuff[strlen(keyword_name)];
	strcpy(keybuff,keyword_name);

	while (strlen(keybuff)<8){strcat(keybuff," ");}
	strcat(keybuff,"= ");
	char buffer[KEYWORD_SIZE]="";
	char *p=strstr(header_buff,keybuff);

	for (int i = 0;i < KEYWORD_SIZE; i++){
		if (i>8 && p[i]!=*"/"){	strncat(buffer,&p[i],1);}
		else if (p[i]==*"/"){break;}
	}
	char *ptr=NULL;
	double keyword=strtod(buffer,&ptr);
	
	if (ptr==buffer) { printf("String couldn't be converted to double\n");return -1;}
	else {	return keyword;}
}
