#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <dirent.h>
#include "numlin.h"


double **create_grids(double min,double max,int n,int l,int axis){
	/*  Produce a grid of values ranging from min to max with n points
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  min  : double : minimal value for the grid
	 *  max  : double : maximal value for the grid
	 *  n    : int    : number of points in the grid 
	 *  l    : int 	  : number of points in the column of the grid
	 *  axis : int    : produce a grid for X if axis=0 and for Y if axis=1 
	 * 
	 */
	 
	double **grid=create_array(l,n);
	double h;
	if (axis==0){ //if grid on x
		h=(max-min)/(n-1);
		for (int i=0;i<l;i++){
			for (int j=0;j<n;j++){grid[i][j]=min+j*h;}
		}
	}
	else {
		h=(max-min)/(l-1);
		for (int i=0;i<l;i++){
			
			for (int j=0;j<n;j++){grid[i][j]=min+i*h;}
		}	
	}
	return grid;
}

void save_array_to_txt(double **array, int nrows, int ncols,char *name){
	/*  Save an array of double of size (nrows, ncols) in a file
	 * 
	 * 
	 * 	Parameters
	 *  ----------
	 * 
	 *  **array : double : array of size (nrows, ncols)
	 *  nrows   : int    : number of rows in the array
	 *  ncols   : int 	 : number of columns in the array
	 *  name    : str    : name of the file
	 * 
	 */
	
	FILE *fp;
	fp=fopen(name, "w+");
	
	for (int i=0;i<nrows;i++){
		for (int j=0;j<ncols;j++){
		fprintf(fp,"%10.12lg\t",array[i][j]);}
		fprintf(fp,"\n");
	}
	fclose(fp);
}

double **create_array(int nrows, int ncols){
	/*  Returns a 2D-array of doubles of size (nrows, ncols)
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  nrows   : int    : number of rows in the array
	 *  ncols   : int 	 : number of columns in the array
	 * 
	 */
	
	double **a=malloc(nrows*sizeof(*a));
	if (a==NULL){
		printf("Failed memory allocation");
		exit(EXIT_FAILURE);}
	for (int i=0;i<nrows;i++){
		a[i]=(double*)malloc(ncols*sizeof(double));
		if (a[i]==NULL){
			printf("Failed memory allocation");
			exit(EXIT_FAILURE);}
	}
	return a;
}

void delete_array(double **a,int nrows){
	 /*  Delete a 2D-array of double of size (nrows, ncols) by freeing the memory
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  a     : double : array of size (nrows, ncols) 
	 *  nrows : int 	 : number of columns in the array
	 * 
	 */
	for (int i=0;i<nrows;i++){
		free(a[i]); 
		a[i] = NULL;
	}
	free(a);
	a=NULL;
}

void delete_array_int(long int **a,int nrows){
    /*  Delete a 2D-array of long ints of size (nrows, ncols) by freeing the memory
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  a     : long int : array of size (nrows, ncols) 
	 *  nrows : int 	 : number of columns in the array
	 * 
	 */
	
	for (int i=0;i<nrows;i++){
		free(a[i]); 
		a[i]=NULL;
	}
	free(a);a=NULL;
}


long int **create_array_int(int nrows, int ncols){
    /*  Returns a 2D-array of long ints of size (nrows, ncols)
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  nrows   : int    : number of rows in the array
	 *  ncols   : int 	 : number of columns in the array
	 * 
	 */
	
	long int **a=calloc(nrows,nrows*sizeof(*a));
	if (a==NULL){
		printf("Failed memory allocation");
		exit(EXIT_FAILURE);}
	for (int i=0;i<nrows;i++){
		a[i]=calloc(ncols,ncols*sizeof(*a[i]));
		if (a[i]==NULL){
			printf("Failed memory allocation");
			exit(EXIT_FAILURE);}
	}
	return a;
}

void save_array_to_txt_int(long int **array, int nrows, int ncols,char *name){
    /*  Save an array of long int of size (nrows, ncols) in a file
	 * 
	 * 
	 * 	Parameters
	 *  ----------
	 * 
	 *  **array : long int : array of size (nrows, ncols)
	 *  nrows   : int      : number of rows in the array
	 *  ncols   : int 	   : number of columns in the array
	 *  name    : str      : name of the file
	 * 
	 */FILE *fp;
	fp=fopen(name, "w+");
	
	for (int i=0;i<nrows;i++){
		for (int j=0;j<ncols;j++){
		fprintf(fp,"%ld\t",array[i][j]);}
		fprintf(fp,"\n");
	}
	fclose(fp);
}

void solve_system(double **X, double **A0,double **B0,int n){
    /*  Solve the system A0 * X = B0 using the Gauss elimination
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **X  : double : vector of size (n, 1) in which the result will be store
	 *  **A0 : double : matrix of size (n, n)
	 *  **B0 : double : vector of size (n, 1)
	 *  n    : int    : number of equations / size of the matrix 
	 * 
	 */
	
	double **A=create_array(n,n);	
	double **B=create_array(n,n);

	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			A[i][j]=A0[i][j];
			B[i][j]=B0[i][j];
		}
	}
	double x;
	int i_max=0;

	for (int j=0;j<n-1;j++){
		//1- search for a maximum pivoting
		for (int i=j+1;i<n;i++){
			i_max=j;
			if (fabs(A[i][j])> fabs(A[i_max][j])){
				i_max=i;
			}
		}
		//2- exchange the values in the lines of A and B
		double *buff=A[j];
		A[j]=A[i_max];
		A[i_max]=buff;
		double *buff1=B[j];
		B[j]=B[i_max];
		B[i_max]=buff1;
		//3- Transform A in a triangular matrix
		for (int i=j+1;i<n;i++){
			x=A[i][j]/A[j][j];
			for (int k=0;k<n;k++){
				A[i][k] = A[i][k]-x*A[j][k];
			}
			B[i][0] = B[i][0]-x*B[j][0];
		}
		//4- Solve the triangular system
		for (int i=0;i<n;i++){
			X[i][0]=0;
		}
		if (A[n-1][n-1]==0){
			printf("System is not triangular ! Abort \n");
			for (int i=0;i<n;i++){X[i][0]=-1;}
			break;
		}
		X[n-1][0] = B[n-1][0]/A[n-1][n-1];
		for (int i=n-2;i>-1;i--){
			double s=0;
			for (int j=i+1;j<n;j++){
				s+=A[i][j]*X[j][0];
			}
			X[i][0]=(B[i][0]-s)/A[i][i];
		}
	}
	delete_array(A,n);
	delete_array(B,n);
}

void find_min_int( long int **A, int n,double *val_min,int index[2]){
	/*  Find the minimal value in a (n, n) matrix 
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **A      : long int : array of size (n, n)
	 *  n        : int      : size of the square array
	 *  *val_min : double   : the minimal value found in the array
	 *  index[2] : int      : array containing the position of the maximum value 
	 * 
	 */
	double buff=INFINITY;

	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			if (A[i][j]<buff){
				buff=A[i][j];
				index[0]=i;
				index[1]=j;
			}		
		}
	}
	*val_min=buff;
}

void find_max_int(long int **A, int n,double *val_max,int index[2]){
	/*  Find the maximum value in a (n, n) matrix 
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **A      : long int : array of size (n, n)
	 *  n        : int      : size of the square array
	 *  *val_max : double   : the maximum value found in the array
	 *  index[2] : int      : array containing the position of the maximum value 
	 * 
	 */
	
	double buff=0;

	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			if (A[i][j]>buff){
				buff=A[i][j];
				index[0]=i;
				index[1]=j;
			}		
		}
	}
	*val_max=buff;
}

void truncate_matrix(long int **T,long int **A, int x0,int y0 ,int box_size){
	/*  Truncates a long int 2D-array into an array of size (box_size, box_size)
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **T		 : long int : truncated array of size (box_size, box_size)
	 *  **array  : long int : original array
	 *  x0       : int      : position of the center of the truncated array
	 *  y0       : int      : position of the center of the truncated array
	 *  box_size : int 	    : size of the truncated array
	 * 
	 */
	int k,l;
	k=0;

	for (int i=(y0-box_size/2);i<(y0+box_size/2);i++){
		l=0;
		for (int j=(x0-box_size/2);j<(x0+box_size/2);j++){
			T[k][l]=A[i][j];
			l++;
		}
		k++;
	}
}


void print_array(double **array, int nrows,  int ncols){
	/*  Prints a 2D-array of long ints of size (nrows, ncols)
	 * 
	 *  Parameters
	 *  ----------
	 * 
	 *  **array : double : array of size (nrows, ncols)
	 *  nrows   : int    : number of rows in the array
	 *  ncols   : int 	 : number of columns in the array
	 * 
	 */
	for (int i=0;i<nrows;i++){
		for (int j=0;j<ncols;j++){printf(" %1.10le ",array[i][j]);}
		printf(" \n");}}
