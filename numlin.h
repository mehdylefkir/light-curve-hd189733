#ifndef NUMLIN_H_INCLUDED
#define NUMLIN_H_INCLUDED

long int **create_array_int(int nrows, int ncols);
double **create_array(int nrows, int ncols);
double **create_grids(double min,double max,int n,int l,int axis);

void solve_system(double **X, double **A0,double **B0,int n);
void truncate_matrix(long int **T,long int **A, int x0,int y0 ,int box_size);
void find_max_int(long int **A, int n,double *val_max,int index[2]);
void find_min_int(long int **A, int n,double *val_min,int index[2]);

void save_array_to_txt(double **array, int nrows, int ncols,char *name);
void save_array_to_txt_int(long int **array, int nrows, int ncols,char *name);
void delete_array(double **a,int nrows);
void print_array(double **array, int nrows,  int ncols);
void delete_array_int(long int **a,int nrows);
#endif
