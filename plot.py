import numpy as np
import matplotlib.pyplot as plt

data=np.loadtxt("results.txt")
jd=data[:,0]
mag=data[:,1]
flux=np.array(mag)[np.array(jd).argsort()]
julianday=np.sort(jd)
fig=plt.figure(figsize=(10,7))
plt.scatter(julianday,flux,marker='+',color="mediumpurple")
plt.xlabel('JD',fontsize=15)
plt.ylabel(r'$N_\mathrm{source}/<N_\mathrm{ref}>$',fontsize=15)
plt.title(r"Courbe de lumière de HD189733",fontsize=18)
fig.savefig("lc_hd189733.pdf")
plt.show()
